# Sonic 1337 #

![Title Screenshot](https://info.sonicretro.org/images/a/a9/Sonic1337.png "Screenshot")

| Version      | Last Release | Status   | System          | Original Game               | Credits |
| ------------ | ------------ | -------- | --------------- | --------------------------- | ------- |
| only version | 2007-05-18   | Complete | Sega Mega Drive | Sonic the Hedgehog (16-bit) | Ultima  |

Sonic 1337 (distributed as Sonic Ultima 1337 in the [GoodGen](https://info.sonicretro.org/GoodXXXX) [ROMset](https://info.sonicretro.org/ROM)) is a parody [hack](https://info.sonicretro.org/Hack) of [_Sonic the Hedgehog_](https://info.sonicretro.org/Sonic_the_Hedgehog_(16-bit)) for the [Sega Mega Drive](https://info.sonicretro.org/Sega_Mega_Drive) by [Ultima](https://info.sonicretro.org/User:Ultima). Created at a time when [palette](https://info.sonicretro.org/Palette)-only hacks were common (but seen as technically lazy and/or sloppy), 1337 took up the valiant cause of attempting to be the "worst hack ever." It utilizes clashing palettes (almost every zone features some form of fuchsia pink and light green), poor collision, and incorrect level layout/object positioning. Amusingly, only the level select features a correct palette.

At one point in time, the Sonic Hacking Contest had a trophy named after this hack, aptly named the 1337!!!!! Trophy: "Worst hack submitted. Why was this even submitted?!" The trophy has since been renamed the Big trophy, starting with the 2011 Hacking Contest.

This hack is similar to the tongue-in-cheek hack [_Sonic in Paynt_](https://info.sonicretro.org/Sonic_in_Paynt) by [sebmal](https://info.sonicretro.org/User:Sebmal).

**_[DOWNLOAD](https://bitbucket.org/sweetfirekitsune/sonic-retro-rom-repository/downloads/Sonic%201337.gen)_**